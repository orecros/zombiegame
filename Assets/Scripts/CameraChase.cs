﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChase : MonoBehaviour
{
    public Transform Focus;

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(Focus.position.x, transform.position.y,Focus.position.z);
    }
}
