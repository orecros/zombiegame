﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public float MoveSpeedMax;
    public float MoveAcceleration;

    Rigidbody rigidbody;


    // Start is called before the first frame update
    void Awake() {
        rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update() {
        DoWalk();

        DoAim();
    }

    void DoWalk() {
        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");

        Vector3 desiredVelocity = new Vector3(moveX, 0, moveY) * MoveSpeedMax;

        rigidbody.velocity = Vector3.MoveTowards(rigidbody.velocity, desiredVelocity, MoveAcceleration * Time.deltaTime);
    }

    void DoAim() {


        Vector3 aimPosition = GetMouseWorldPos();
        Vector3 aimDirection = aimPosition - transform.position;

        //make the ship point towards the cursor
        var yaw = Mathf.Atan2(aimDirection.z, aimDirection.x);
        transform.rotation = Quaternion.Euler(90, yaw * Mathf.Rad2Deg, 0);
    }

    Vector3 GetMouseWorldPos() {
        Vector3 mousePos = Input.mousePosition;
        mousePos.y = -1 * Camera.main.transform.position.y;

        return Camera.main.ScreenToWorldPoint(mousePos);
    }
}
